﻿<?xml version="1.0" encoding="utf-8"?>
<package xmlns="http://schemas.microsoft.com/packaging/2015/06/nuspec.xsd">
  <metadata>
    <id>librewolf</id>
    <version>${CHOCO_VERSION}</version>
    <title>LibreWolf</title>
    <authors>LibreWolf contributors</authors>
    <projectUrl>https://librewolf.net</projectUrl>
    <iconUrl>https://glcdn.githack.com/librewolf-community/branding/-/raw/master/icon/icon.svg</iconUrl>
    <licenseUrl>https://codeberg.org/librewolf/source/src/branch/main/LICENSE</licenseUrl>
    <packageSourceUrl>https://codeberg.org/librewolf/bsys6/src/branch/master/assets/choco</packageSourceUrl>
    <releaseNotes>https://gitlab.com/librewolf-community/browser/bsys6/-/releases</releaseNotes>
    <docsUrl>https://librewolf.net/docs</docsUrl>
    <bugTrackerUrl>https://codeberg.org/librewolf/issues</bugTrackerUrl>
    <projectSourceUrl>https://codeberg.org/librewolf</projectSourceUrl>
    <tags>browser librewolf firefox fork</tags>
    <summary>A fork of Firefox, focused on privacy, security and freedom.</summary>
    <description>This project is an independent fork of Firefox, with the primary goals of privacy, security and user freedom.

LibreWolf is designed to increase protection against tracking and fingerprinting techniques, while also including a few security improvements. This is achieved through our privacy and security oriented settings and patches. LibreWolf also aims to remove all the telemetry, data collection and annoyances, as well as disabling anti-freedom features like DRM.

**LibreWolf is NOT associated with Mozilla or its products.**

## Features

- **No Telemetry** — No experiments, adware, annoyances, or unnecessary distractions.
- **Private Search** — Privacy-conscious search providers: [DuckDuckGo](https://duckduckgo.com/), [Searx](https://searx.be/), [Qwant](https://www.qwant.com/) and more.
- **Content Blocker Included** — [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin) is already included for your convenience.
- **Enhanced Privacy** — Hardened to maximize privacy, without sacrificing usability.
- **Fast Updates** — LibreWolf is always built from the latest Firefox stable source, for up-to-date security and features along with stability.
- **Open Source** — Everyone can participate in the development of LibreWolf. Join us on [GitLab](https://codeberg.org/librewolf) and [Matrix](https://matrix.to/#/#librewolf:matrix.org).

For more details read the full [feature list](https://librewolf.net/docs/features).
</description>
    <dependencies>
      <dependency id="vcredist140" version="14.0" />
    </dependencies>
  </metadata>
  <files>
    <file src="tools/**" target="tools" />
  </files>
</package>